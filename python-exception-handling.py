#### Exception Handling (checking three exception error)

try:
    arr = [1,2,3,4,5,6,7,8]
    print(arr[9])

except ArithmeticError as e:
    print("ArithmeticError : ",e)

except BufferError as e:
    print("BufferError : ",e)

except LookupError as e:
    print("LookupError : ",e)

else:
    print("Success! no exception occur !!!")
