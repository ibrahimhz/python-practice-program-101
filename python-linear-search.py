# Pyhon Linear Search

def linear_search(mylist, item):
    for i in range(len(mylist)):
        if mylist[i]==item:
            return i
    
    return -1

mylist = [1,7,6,5,8]
print("Elements in list: ",mylist)
x = int(input("Enter searching element: "))
result = linear_search(mylist,x)
if result == -1:
    print("Elements not found in the list")
else:
    print("Elements "+str(x)+" is found at position %d"%(result))

