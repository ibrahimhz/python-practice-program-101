# Example -1: 
# Program to get n elements from user with unique keys and add the corresponding values to the dictionary
# Program to add students marks in the dictionary, it only adds when students name is unique

students_marks = {}
name_mark_list = list(map(str, input("Enter the students name and marks separated by space (eg: bob 35 jimmy 80 ... ) : ").split(' ')))
for x in range(0, len(name_mark_list), 2):
    if name_mark_list[x] not in students_marks:
        students_marks[name_mark_list[x]] = name_mark_list[x+1]

for x in students_marks:
    print(x," scored ",students_marks[x],"marks")
###########################################################################

# Example -2 : 
# Program to get n elements from user with unique keys and add the corresponding values to the dictionary
# Program to add students marks in the dictionary, it only adds when students enrollment_number is unique

students_marks = {}
name_mark_list = list(map(int, input("Enter the students enrollment_number and marks separated by space (eg: 10100 35 10101 80 ... ) : ").split(' ')))
for x in range(0, len(name_mark_list), 2):
    if name_mark_list[x] not in students_marks:
        students_marks[name_mark_list[x]] = name_mark_list[x+1]

for x in students_marks:
    print(x," scored ",students_marks[x],"marks")

###########################################################################
